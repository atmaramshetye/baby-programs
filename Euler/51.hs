import Data.Char

numberFromPattern :: [Char] -> Int -> Int
numberFromPattern [] _ = 0
numberFromPattern xs y = if (last xs) == '*'
                            then y + 10 * numberFromPattern (init xs) y
                            else digitToInt (last xs) + 10 * numberFromPattern (init xs) y
