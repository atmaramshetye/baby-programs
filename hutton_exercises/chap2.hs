-- Exercise 3
n = a `div` length xs
    where
        a = 10
        xs = [1,2,3,4,5]

-- Exercise 4
mylast1 [x] = x
mylast1 x = mylast1 (tail x)

mylast2 [x] = x
mylast2 x = mylast2 (drop 1 x)

-- Exercise 5
myinit1 [x] = []
myinit1 x = [head x] ++ myinit1 (tail x)

myinit2 [x] = []
myinit2 x = take 1 x ++ myinit2 (drop 1 x)
