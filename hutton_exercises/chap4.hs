-- Exercise 1
halve :: [a] -> ([a], [a])
halve [] = error "Its dumb to try to halve an empty list. I am not dumb"
halve xs = if (rem (length xs)) 2 == 0 
            then (take (div (length xs) 2) xs, drop (div (length xs) 2) xs)
            else error "Cannot be unfair to one of the lists by splitting unevenly"

-- Exercise 2
safetail_conditional :: [a] -> [a]
safetail_conditional xs = if null xs
                            then xs
                            else tail xs

safetail_guarded :: [a] -> [a]
safetail_guarded xs | null xs = xs
                    | otherwise = tail xs

safetail_pattern :: [a] -> [a]
safetail_pattern [] = []
safetail_pattern xs = tail xs

-- Exercise 3
v1 :: Bool -> Bool -> Bool
v1 True True = True
v1 True False = True
v1 False True = True
v1 False False = False

v2 :: Bool -> Bool -> Bool
v2 False False = False
v2 _ _ = True

v3 :: Bool -> Bool -> Bool
v3 False b = b
v3 True _ = True

v4 :: Bool -> Bool -> Bool
v4 b c | b == c = b
       | otherwise = True

-- Exercise 4
and' :: Bool -> Bool -> Bool
and' a b = if (a == b) && (a == True)
            then True
            else False

-- Exercise 5
and'' :: Bool -> Bool -> Bool
and'' a b = if a == True
            then b
            else False

-- Exercise 6
mult'' = \x -> \y -> \z -> (x * y * z)
