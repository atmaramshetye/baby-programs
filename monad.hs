putStrLn' str = do
                putStr str
                putChar '\n'

putQStrLn = do
            str <- getLine
            putChar '"'
            putStr str
            putChar '"'
