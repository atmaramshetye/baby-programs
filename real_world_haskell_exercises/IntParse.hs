import Data.Char

loop acc [] = acc
loop acc (x:xs) = let acc' = acc * 10 + digitToInt x
                    in loop acc' xs

loop' acc [] = acc
loop' acc (x:xs) = loop' (acc * 10 + digitToInt x) xs

asInt :: String -> Int
asInt [] = 0
asInt xs = digitToInt (last xs) + 10 * asInt (init xs)
