-- file: ch04.hs
import Data.List
import Data.Char

import System.Environment (getArgs)
-- Exercise 1
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:y) = Just x

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (x:y) = Just (y)

safeLast :: [a] -> Maybe a
safeLast [] = Nothing
safeLast (x:[]) = Just x
safeLast (x:y) = safeLast y

safeInit :: [a] -> Maybe [a]
safeInit [] = Nothing
safeInit (x:[]) = Just []
safeInit (x:y) = Just (x : myInit y)
    where myInit (x:y) = if not (null y)
                        then x : (myInit y)
                        else []

-- Exercise 2
splitWith :: (a -> Bool) -> [a] -> [[a]]
splitWith predicate [] = []
splitWith predicate (x:xs) = if not (predicate x)
                            then ([x] : (splitWith predicate xs))
                            else ((x:y) : (splitWith predicate (concat ys)))
                                where (y:ys) = (splitWith predicate xs)

-- Exercise 3

firstWords :: String -> String
firstWords [] = []
firstWords cs = unlines (map (head . words) (splitLines cs))

-- Exercise 4

myHead :: String -> Char
myHead [] = ' '
myHead (x:xs) = x

myTail :: String -> String
myTail [] = ""
myTail (x:xs) = xs

transpose' :: [String] -> [String]
transpose' [] = []
transpose' x = if all null x
    then []
    else (map myHead x : transpose' (map myTail x))

transposeLines :: String -> String
transposeLines [] = []
transposeLines cs = unlines $ transpose' $ splitLines cs

splitLines :: String -> [String]
splitLines [] = []
splitLines cs = 
    let (pre, suf) = break isLineTerminator cs
    in pre : case suf of
        ('\r' : '\n' : rest)    -> splitLines rest
        ('\r' : rest)           -> splitLines rest
        ('\n' : rest)           -> splitLines rest
        _                       -> []

isLineTerminator c = c == '\r' || c == '\n'

interactWith function inputFile outputFile = do
  input <- readFile inputFile
  writeFile outputFile (function input)

main = mainWith myFunction
  where mainWith function = do
          args <- getArgs
          case args of
            [input,output] -> interactWith function input output
            _ -> putStrLn "error: exactly two arguments needed"

        myFunction = transposeLines

-- fold
-- Exercise 1

asInt_fold :: String -> Int
asInt_fold ('-':xs) = negate (asInt_fold xs)
asInt_fold x = foldl' step 0 x
                where step acc y = acc * 10 + digitToInt y

-- Exercise 3
concat_fold :: [[a]] -> [a]
concat_fold xs = foldr step [] xs
                where step y acc = foldr (:) acc y

-- Exercise 4
takeWhile_rec :: (a -> Bool) -> [a] -> [a]
takeWhile_rec predicate [] = []
takeWhile_rec predicate (x:xs) = if predicate x
                                then (x : takeWhile_rec predicate xs)
                                else []

takeWhile_fold :: (a -> Bool) -> [a] -> [a]
takeWhile_fold predicate xs = foldr step [] xs
                            where step y acc = if predicate y
                                                then (y : acc)
                                                else []

-- Exercise 5
groupBy' :: (a -> a -> Bool) -> [a] -> [[a]]
groupBy' compare xs = foldr step [] xs
                            where
                            step x [] = [[x]]
                            step x (y:ys) = if compare x (head y)
                                            then (x:y) : ys
                                            else [x] : (y:ys)

-- Exercise 6
any' :: (a -> Bool) -> [a] -> Bool
any' predicate xs = foldl' step False xs
                where step acc y = if predicate y
                                    then True
                                    else acc

cycle' :: [a] -> [a]
cycle' [] = error "Cycle not defined for empty list"
cycle' xs = foldr step xs xs
            where   step y [] = foldr step [] xs
                    step y acc = foldr (:) xs xs
